# **¿Qué es Sass?**

Es una herramienta muy útil para los diseñadores web, facilitando el desarrollado a la hora de trabajar en CSS gracias a sus múltiples funcionalidades.

Sass permite usar variables, bloques de código reutilizable, mixins y funciones. Posteriormente a través de la línea de comandos es posible traducir o complicar este código al CSS.

## **Ventaja**

La principal ventaja de SASS es la posibilidad de convertir los CSS en algo dinámico. Permite trabajar mucho más rápido en la creación de código con la posibilidad de crear funciones que realicen ciertas operaciones matemáticas y reutilizar código gracias a los mixins, variables que nos permiten guardar valores. SASS, en definitiva, se convierte en tu mejor ayudante.

## **Ejemplo**

-   Crearemos un archivo en la carpeta theme, con el nombre de colores.scss.

![sass-vscode](./imagenes-css/vscode-sass.png)

-   El archivo contendrá esta variables definidas.

```
$color-buttom-facebook: #488aff;

$color-buttom-google: #f53d3d;

$anaranjado: #ffaa19;

$color-card-notificaciones:  #488aff;

$color-buttom-registrar-lugar: #a200ff;

```

![sass-archivo](./imagenes-css/archivo-sass.png)

-   Para utilizar estas variables se debe importar de la siguiente forma:

```
@import "../../theme/colores.scss";

.button-facebook {
    border-color: $color-buttom-facebook;
    color: $color-buttom-facebook;
    background-color: #488aff38;
    margin-bottom: 1.5rem;
    height: 4.8rem;
}

.button-google {
    border-color: $color-buttom-google;
    color: $color-buttom-google;
    background-color: #f53d3d36;
    height: 4.8rem;
}
```

![sass-import](./imagenes-css/import-sass.png)

- Corremos la aplicación y veremos como se aplican los colores de las variables instanciadas.

![sass-app](./imagenes-css/app-sass.png)



<a href="https://www.facebook.com/jonathan.parra.56" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://sguru.org/wp-content/uploads/2018/02/Facebook-PNG-Image-71244.png" title="Sígueme en Twitter"/> Jonathan Parra </a><br>
<a href="https://www.linkedin.com/in/jonathan-parra-a89a68162/" target="_blank"><img alt="Sígueme en LinkedIn" height="35" width="35" src="https://4.bp.blogspot.com/-0KtSvK3BydE/XCrIzgI3RqI/AAAAAAAAH_w/n_rr5DS92uk9EWEegcxeqAcSkV36OWEOgCLcBGAs/s1600/linkedin.png" title="Sígueme en LinkedIn"/> Jonathan Parra</a><br>
<a href="https://www.instagram.com/Choco_20jp/" target="_blank"><img alt="Sígueme en Instagram" height="35" width="35" src="https://4.bp.blogspot.com/-Ilxti1UuUuI/XCrIy6hBAcI/AAAAAAAAH_k/QV5KbuB9p3QB064J08W2v-YRiuslTZnLgCLcBGAs/s1600/instagram.png" title="Sígueme en Instagram"/> Choco_20jp</a><br>